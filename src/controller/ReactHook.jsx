import React, { useState } from "react";
import useDataAPI from "../customHook/useDataAPI";

const ReactHook = (props) => {
  const [query, setQuery] = useState("redux");
  const [
    { isError, isLoading, data },
    fetchData,
  ] = useDataAPI(`https://hn.algolia.com/api/v1/search?query=${query}`, {
    hits: [],
  });
  return (
    <>
      <input
        type="text"
        onChange={(e) => {
          setQuery(e.target.value);
        }}
      />
      <button
        type="button"
        onClick={() => {
          fetchData(`https://hn.algolia.com/api/v1/search?query=${query}`);
        }}
      >
        Search
      </button>
      <div>
        {isError && <p>Something went wrong</p>}
        {isLoading ? (
          <p>Loading</p>
        ) : (
          <ul>
            {data.hits.map((item) => (
              <li key={item.objectID}>
                <a href={item.url}>{item.title}</a>
              </li>
            ))}
          </ul>
        )}
      </div>
    </>
  );
};

export default ReactHook;
