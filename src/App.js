import logo from "./logo.svg";
import "./App.css";
import ReactHook from "./controller/ReactHook";
function App() {
  return (
    <div className="App">
      <ReactHook />
    </div>
  );
}

export default App;
