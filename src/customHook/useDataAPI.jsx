import React, { useState, useReducer, useEffect } from "react";
import axios from "axios";

const actionTypes = {
  FETCH_INIT: "FETCH_INIT",
  FETCH_SUCCESS: "FETCH_SUCCESS",
  FETCH_FAILURE: "FETCH_FAILURE",
};

const updateObject = (oldState, objectUpdate) => ({
  ...oldState,
  ...objectUpdate,
});

const actionCreator = {
  fetchInit: () => ({
    type: actionTypes.FETCH_INIT,
  }),
  fetchSuccess: (data) => ({
    type: actionTypes.FETCH_SUCCESS,
    payload: data,
  }),
  fetchFailure: () => ({
    type: actionTypes.FETCH_FAILURE,
  }),
};

const dataFetchReducer = (state, action) => {
  switch (action.type) {
    case actionTypes.FETCH_INIT:
      return updateObject(state, { isLoading: true, isError: false });
    case actionTypes.FETCH_SUCCESS:
      return updateObject(state, {
        isLoading: false,
        isError: false,
        data: action.payload,
      });
    case actionTypes.FETCH_FAILURE:
      return updateObject(state, { isLoading: false, isError: true });
    default:
      return state;
  }
};

const useDataAPI = (initialUrl, InitialData) => {
  const [url, setUrl] = useState(initialUrl);
  const [state, dispatch] = useReducer(dataFetchReducer, {
    isLoading: false,
    isError: false,
    data: InitialData,
  });
  useEffect(() => {
    const fetchData = async () => {
      dispatch(actionCreator.fetchInit());
      try {
        const result = await axios.get(url);
        dispatch(actionCreator.fetchSuccess(result.data));
      } catch (error) {
        dispatch(actionCreator.fetchFailure());
      }
    };
    fetchData();
  }, [url]);

  return [state, setUrl];
};

export default useDataAPI;
